# Getting Started

To run the development server, run the following:

```bash
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser

## Running tests

```bash
npm test
```

## Building

To build, run the following command:

```bash
npm run build
```

You can now run

```bash
npm start
```

## Deploying on Vercel

To deploy, the following command will do a production build and deployment:

```
npm ci
npm run deploy
```

## Assumptions

Deploying to Vercel is allowed and there is no need to use AWS/Azure/etc. This allows for straightforward deployments and limited infrastructure code while still supporting functionality such as serverless functions and edge caching.

Webpack 5 is desired for future maintainability
