import stopsApi from '../../pages/api/stops/[...slug]'
import nockBack from './nockBack'

test('stops api', async () => {
  const fixture = await nockBack('stops.json')
  const req = {
    query: {
      slug: ['901', 'NORTHBOUND'],
    },
  }
  const res = {
    json: jest.fn(),
    setHeader: jest.fn(() => res),
    status: jest.fn(() => res),
  }
  await stopsApi(req as any, res)
  expect(res.status).toHaveBeenCalledWith(200)
  const json = res.json.mock.calls[0][0]
  const { stops } = json
  expect(stops?.length).toBeGreaterThan(1)
  expect(stops[0].id).toBeTruthy()
  expect(stops[0].name).toBeTruthy()
  fixture.nockDone()
})
