import directionsApi from '../../pages/api/directions/[id]'
import nockBack from './nockBack'

test('directions api', async () => {
  const fixture = await nockBack('directions.json')
  const req = {
    query: {
      id: '901',
    },
  }
  const res = {
    json: jest.fn(),
    setHeader: jest.fn(() => res),
    status: jest.fn(() => res),
  }
  await directionsApi(req as any, res)
  expect(res.status).toHaveBeenCalledWith(200)
  const json = res.json.mock.calls[0][0]
  const { directions } = json
  expect(directions?.length).toBeGreaterThan(1)
  expect(directions[0].id).toBeTruthy()
  expect(directions[0].name).toBeTruthy()
  fixture.nockDone()
})
