import transitRoutes from '../../pages/api/transitRoutes'
import nockBack from './nockBack'

test('transitRoutes api', async () => {
  const fixture = await nockBack('transite-routes.json')
  const req = {}
  const res = {
    json: jest.fn(),
    setHeader: jest.fn(() => res),
    status: jest.fn(() => res),
  }
  await transitRoutes(req, res)
  expect(res.status).toHaveBeenCalledWith(200)
  const json = res.json.mock.calls[0][0]
  const { routes } = json
  expect(routes?.length).toBeGreaterThan(1)
  expect(routes[0].id).toBeTruthy()
  expect(routes[0].name).toBeTruthy()
  fixture.nockDone()
})
