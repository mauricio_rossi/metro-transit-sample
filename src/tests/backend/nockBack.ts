import { back as nockBack } from 'nock'

nockBack.fixtures = __dirname + '/nockFixtures'
nockBack.setMode(process.env.CI ? 'lockdown' : 'record')

export default nockBack
