import { render, waitFor } from '@testing-library/react'
import TransitRoutesPage from '../../pages/transit-routes'
import { SWRConfig } from 'swr'

const renderTransitRoutesPage = () => {
  return render(
    <SWRConfig value={{ dedupingInterval: 0 }}>
      <TransitRoutesPage />
    </SWRConfig>
  )
}

describe('Transit Routes Page', () => {
  test('should render the basics', async () => {
    const { getByTestId } = renderTransitRoutesPage()
    expect(await waitFor(() => getByTestId('header'))).toBeVisible()
    expect(await waitFor(() => getByTestId('header-logo'))).toBeVisible()
    expect(await waitFor(() => getByTestId('page-title'))).toBeVisible()
    expect(await waitFor(() => getByTestId('page-sub-title'))).toBeVisible()
  })
  test('should render routes', async () => {
    const { getByText } = renderTransitRoutesPage()
    expect(await waitFor(() => getByText('METRO Blue Line'))).toBeVisible()
  })
})
