import { render, waitFor } from '@testing-library/react'
import DirectionsPage from '../../pages/directions/[id]'
import { SWRConfig } from 'swr'

jest.mock('next/dist/client/router', () => ({
  useRouter: () => ({
    query: {
      id: '901',
    },
    prefetch: jest.fn(() => Promise.resolve(true)),
  }),
}))

const renderDirectionsPage = () => {
  return render(
    <SWRConfig value={{ dedupingInterval: 0 }}>
      <DirectionsPage />
    </SWRConfig>
  )
}

describe('Directions Page', () => {
  test('should render the basics', async () => {
    const { getByTestId } = renderDirectionsPage()
    expect(await waitFor(() => getByTestId('header'))).toBeVisible()
    expect(await waitFor(() => getByTestId('header-logo'))).toBeVisible()
    expect(await waitFor(() => getByTestId('page-title'))).toBeVisible()
    expect(await waitFor(() => getByTestId('page-sub-title'))).toBeVisible()
  })
  test('should render directions', async () => {
    const { getByText } = renderDirectionsPage()
    expect(await waitFor(() => getByText('Northbound'))).toBeVisible()
  })
})
