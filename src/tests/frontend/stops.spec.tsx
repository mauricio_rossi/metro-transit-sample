import { render, waitFor } from '@testing-library/react'
import StopsPage from '../../pages/stops/[...slug]'
import { SWRConfig } from 'swr'

jest.mock('next/dist/client/router', () => ({
  useRouter: () => ({
    query: {
      slug: ['901', 'NORTHOUND'],
    },
    prefetch: jest.fn(() => Promise.resolve(true)),
  }),
}))

const renderStopsPage = () => {
  return render(
    <SWRConfig value={{ dedupingInterval: 0 }}>
      <StopsPage />
    </SWRConfig>
  )
}

describe('Stops Page', () => {
  test('should render the basics', async () => {
    const { getByTestId } = renderStopsPage()
    expect(await waitFor(() => getByTestId('header'))).toBeVisible()
    expect(await waitFor(() => getByTestId('header-logo'))).toBeVisible()
    expect(await waitFor(() => getByTestId('page-title'))).toBeVisible()
    expect(await waitFor(() => getByTestId('page-sub-title'))).toBeVisible()
  })
  test('should stops', async () => {
    const { getByText } = renderStopsPage()
    expect(
      await waitFor(() => getByText('Mall of America Station'))
    ).toBeVisible()
  })
})
