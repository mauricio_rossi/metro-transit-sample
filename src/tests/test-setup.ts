import { rest } from 'msw'
import { setupServer } from 'msw/node'
import 'whatwg-fetch'
import '@testing-library/jest-dom'

import { back as nockBack } from 'nock'
nockBack.fixtures = __dirname + '/nockFixtures'
nockBack.setMode(process.env.CI ? 'lockdown' : 'record')

const handlers = [
  rest.get('/api/transitRoutes', async (_req, res, ctx) => {
    return res(
      ctx.json({
        routes: [
          {
            id: '901',
            name: 'METRO Blue Line',
            type: 'RAIL',
          },
          {
            id: '906',
            name: 'Airport Shuttle',
            type: 'GROUND_TRANSIT',
          },
          {
            id: '923',
            name: 'METRO C Line',
            type: 'RAPID_TRANSIT',
          },
        ],
      })
    )
  }),
  rest.get('/api/stops/:routeId/:direction', async (_req, res, ctx) => {
    return res(
      ctx.json({
        stops: [
          {
            id: 'MAAM',
            name: 'Mall of America Station',
          },
          {
            id: '28AV',
            name: '28th Ave Station',
          },
        ],
      })
    )
  }),
  rest.get('/api/directions/:routeId', async (_req, res, ctx) => {
    return res(
      ctx.json({
        directions: [
          {
            id: 'NORTHBOUND',
            name: 'Northbound',
          },
          {
            id: 'SOUTHBOUND',
            name: 'Southbound',
          },
        ],
      })
    )
  }),
]

const server = setupServer(...handlers)

beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())
