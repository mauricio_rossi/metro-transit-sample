import Header from './header'
import styled from 'styled-components'
import { desktop } from '../styles/breakpoints'
import { FunctionComponent } from 'react'

const PageContainer: FunctionComponent = (props) => {
  return (
    <>
      <Header />
      <PageContents>{props.children}</PageContents>
    </>
  )
}

export default PageContainer

const PageContents = styled.main`
  padding: 15px;
  ${desktop} {
    padding: 35px 30px;
    max-width: 900px;
    margin: 0 auto;
  }
`
