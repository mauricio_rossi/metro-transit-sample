import Link from 'next/link'
import styled from 'styled-components'
import ChevronRight from '../assets/chevronRight'

export interface ListItemData {
  id: string
  href: string
  content: React.ReactNode
}

const List = styled.ul`
  width: 100%;
  position: relative;
  margin: 0;
  padding: 0;
  list-style-type: none;
`

const ListItemChevron = styled.span`
  position: absolute;
  right: 0;
  top: 50%;
  transform: translateY(-50%);
  z-index: 2;
  svg path {
    fill: white;
  }
`

const highlight = `
  border-bottom-color: transparent;
  color: black;
  &:before {
    opacity: 1;
    visibility: visible;
  }
  ${ListItemChevron} {
    svg path {
      fill: black;
    }
  }
`

const ListItemElem = styled.a`
  display: flex;
  align-items: center;
  padding: 10px 0;
  min-height: 50px;
  color: white;
  text-decoration: none;
  border-bottom: 1px solid rgba(255, 255, 255, 0.1);
  -webkit-tap-highlight-color: transparent;
  &:before {
    content: ' ';
    display: block;
    position: absolute;
    top: 0;
    bottom: 0;
    right: -15px;
    left: -15px;
    background: white;
    box-shadow: 0px 7px 8px rgb(0 0 0 / 20%);
    z-index: 1;
    opacity: 0;
    visibility: hidden;
  }
  @media (hover: none) {
    &:active {
      ${highlight}
    }
  }
  @media (hover: hover) {
    &:hover {
      ${highlight}
    }
  }
`

const ListItemLi = styled.li`
  margin: 0;
  padding: 0;
  position: relative;
  &:last-child ${ListItemElem} {
    border-bottom: none;
  }
`

const ListItemText = styled.div`
  position: relative;
  z-index: 2;
  display: inline-flex;
  align-items: center;
`

interface ItemListProps {
  items: ListItemData[]
}

export default function MetroList(props: ItemListProps) {
  const { items } = props
  return (
    <List>
      {items.map((item) => (
        <ListItemLi key={item.id} data-testid={`list-item-${item.id}`}>
          <Link href={item.href} passHref={true}>
            <ListItemElem>
              <ListItemText>{item.content}</ListItemText>
              <ListItemChevron>
                <ChevronRight />
              </ListItemChevron>
            </ListItemElem>
          </Link>
        </ListItemLi>
      ))}
    </List>
  )
}
