import styled from 'styled-components'

export default function LoadingIndicator() {
  return (
    <SpinnerContainer>
      <Spinner viewBox="0 0 50 50">
        <SpinnerCircle cx="25" cy="25" r="20" fill="none" />
      </Spinner>
    </SpinnerContainer>
  )
}

const SpinnerContainer = styled.div`
  height: 50px;
  position: relative;
`

// Below CSS was based on a Codepen
// https://codepen.io/supah/pen/BjYLdW

const Spinner = styled.svg`
  animation: rotate 2s linear infinite;
  z-index: 2;
  position: absolute;
  top: 50%;
  left: 50%;
  margin-top: -25px;
  margin-left: -25px;
  width: 50px;
  height: 50px;
  @keyframes rotate {
    100% {
      transform: rotate(360deg);
    }
  }
  @keyframes dash {
    0% {
      stroke-dasharray: 1, 150;
      stroke-dashoffset: 0;
    }
    50% {
      stroke-dasharray: 90, 150;
      stroke-dashoffset: -35;
    }
    100% {
      stroke-dasharray: 90, 150;
      stroke-dashoffset: -124;
    }
  }
`

const SpinnerCircle = styled.circle`
  stroke: white;
  stroke-linecap: round;
  stroke-width: 3px;
  animation: dash 1.5s ease-in-out infinite;
`
