import Link from 'next/link'
import styled from 'styled-components'
import { desktop } from '../styles/breakpoints'

const HeaderElem = styled.header`
  margin: 0;
  padding: 15px;
  ${desktop} {
    padding: 15px 30px;
  }
`

// Placeholder, non-copyright infringing logo
const Logo = styled.a`
  color: white;
  text-decoration: none;
  font-weight: bold;
  font-size: 24px;
`

export default function Header() {
  return (
    <HeaderElem data-testid="header">
      <Link href="/" passHref={true}>
        <Logo data-testid="header-logo" title="MetroTransit">
          M
        </Logo>
      </Link>
    </HeaderElem>
  )
}
