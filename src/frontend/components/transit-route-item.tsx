import styled from 'styled-components'
import Train from '../assets/train'
import Bus from '../assets/bus'

interface TransitRouteProps {
  type: string
  name: string
}

const GroundTransit = styled.span`
  width: 36px;
  height: 36px;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  margin-right: 10px;
  background: #fbc748;
  border-radius: 50%;
  color: #f4d233;
  svg {
    width: 18px;
    height: auto;
  }
  svg path {
    fill: white;
  }
`

const Rail = styled(GroundTransit)`
  color: white;
  background: #0070ff;
`

const RapidTransit = styled(GroundTransit)`
  color: white;
  background: #fe3900;
`

export default function TransitRouteItem(route: TransitRouteProps) {
  return (
    <>
      {route.type === 'GROUND_TRANSIT' && (
        <GroundTransit>
          <Bus />
        </GroundTransit>
      )}
      {route.type === 'RAPID_TRANSIT' && (
        <RapidTransit>
          <Bus />
        </RapidTransit>
      )}
      {route.type === 'RAIL' && (
        <Rail>
          <Train />
        </Rail>
      )}
      {route.name}
    </>
  )
}
