import useSWR from 'swr'

const fetcher = (args) => fetch(args).then((res) => res.json())

interface RoutesResponse {
  routes: {
    id: string
    name: string
    type: string
  }[]
}

interface DirectionResponse {
  directions: {
    id: string
    name: string
  }[]
}

interface StopsResponse {
  stops: {
    id: string
    name: string
  }[]
}

export const useTransitRoutes = () => {
  return useSWR<RoutesResponse>('/api/transitRoutes', fetcher)
}

export const useDirections = (id?: string | string[]) => {
  if (Array.isArray(id)) {
    throw new Error('Invalid id parameter')
  }
  return useSWR<DirectionResponse>(id ? `/api/directions/${id}` : null, fetcher)
}

export const useStops = (
  id?: string | string[],
  direction?: string | string[]
) => {
  if (Array.isArray(id) || Array.isArray(direction)) {
    throw new Error('Invalid id or direction parameter')
  }
  return useSWR<StopsResponse>(
    id && direction ? `/api/stops/${id}/${direction}` : null,
    fetcher
  )
}
