import styled from 'styled-components'
import { desktop } from './breakpoints'

export const PageTitle = styled.h1`
  color: white;
  font-weight: 100;
  font-size: 2rem;
  margin: 0 0 15px 0;
  padding: 0;
  ${desktop} {
    font-size: 48px;
  }
`

export const PageSubTitle = styled.h2`
  margin: 0 0 35px 0;
  padding: 0;
  font-size: 14px;
  font-weight: bold;
  color: white;
  ${desktop} {
    font-size: 18px;
  }
`
