import { createGlobalStyle } from 'styled-components'
import { desktop, tablet } from './breakpoints'

const GlobalStyles = createGlobalStyle`
  html,
  body {
    padding: 0;
    margin: 0;
    font-family: 'Raleway', sans-serif;
    font-size: 13px;
    height: 100%;
    ${tablet} {
      font-size: 14px;
    }
    ${desktop} {
      font-size: 16px;
    }
  }

  body {
    background: linear-gradient(130deg, #02418e, #021c3c) no-repeat;
    background-attachment: fixed;
    color: white;
    font-size: 14px;
    padding: env(safe-area-inset-top) env(safe-area-inset-right) env(safe-area-inset-bottom) env(safe-area-inset-left);
  }

  * {
    box-sizing: border-box;
  }

`

export default GlobalStyles
