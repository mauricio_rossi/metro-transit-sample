import axios from 'axios'

export enum TRANSIT_TYPE {
  RAIL = 'RAIL',
  RAPID_TRANSIT = 'RAPID_TRANSIT',
  GROUND_TRANSIT = 'GROUND_TRANSIT',
}

export enum TransitDirection {
  NORTHBOUND = 'NORTHBOUND',
  SOUTHBOUND = 'SOUTHBOUND',
  WESTBOUND = 'WESTBOUND',
  EASTBOUND = 'EASTBOUND',
}

// Most transit types are bound to be regular "ground_transit",
// with just a handful of exceptions

const transitTypes: { [key: string]: TRANSIT_TYPE } = {
  '901': TRANSIT_TYPE.RAIL,
  '902': TRANSIT_TYPE.RAIL,
  '903': TRANSIT_TYPE.RAPID_TRANSIT,
  '921': TRANSIT_TYPE.RAPID_TRANSIT,
  '923': TRANSIT_TYPE.RAPID_TRANSIT,
  '888': TRANSIT_TYPE.RAIL,
  '887': TRANSIT_TYPE.RAIL,
}

const client = axios.create({
  baseURL: 'https://svc.metrotransit.org/nextripv2/',
})

export const getTransitRoutes = async () => {
  const res = await client.get('/routes')
  const routes = res.data.map((route) => {
    // Default to GROUND_TRANSIT
    let type: TRANSIT_TYPE =
      transitTypes[route.route_id] || TRANSIT_TYPE.GROUND_TRANSIT
    return {
      id: route.route_id,
      name: route.route_label,
      type,
    }
  })
  return routes
}

export const getDirections = async (routeId: string) => {
  // All data minus the "metro id" (0 / 1) as opposed to "northbound" and "southbound"
  const res = (await getDirectionsData(routeId)).map((direction) => {
    return {
      id: direction.id,
      name: direction.name,
    }
  })
  return res
}

const getDirectionsData = async (routeId: string) => {
  const res = await client.get(`/directions/${routeId}`)
  const data = res.data.map((item) => {
    // Metro uses ids such as 0 and 1 for northbound/southbound and eastbound/westbound
    // Exposing "northbound" and "southbound" instead within our own API and front-end
    const direction = item.direction_name.toUpperCase()
    return {
      direction,
      name: item.direction_name,
      id: direction,
      metroId: item.direction_id,
    }
  })
  return data
}

export const getStops = async (
  routeId: string,
  direction: TransitDirection
) => {
  const directionObj = (await getDirectionsData(routeId)).find(
    (item) => item.direction === direction
  )
  if (!directionObj) {
    throw new Error('INVALID_DIRECTION')
  }
  const directionId = directionObj.metroId
  const res = await client.get(`/Stops/${routeId}/${directionId}`)
  return res.data.map((item) => {
    return {
      id: item.place_code,
      name: item.description,
    }
  })
}
