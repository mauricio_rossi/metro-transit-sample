import { NextApiResponse } from 'next'

export default function cachedResponse(res: NextApiResponse, data: any) {
  // Cache all responses using edge cache
  // More info below:
  // https://vercel.com/docs/serverless-functions/edge-caching
  res.setHeader('Cache-Control', 's-maxage=86400, stale-while-revalidate=3600')
  res.status(200).json(data)
}
