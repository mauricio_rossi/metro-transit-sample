import { NextApiResponse } from 'next'

export default function badRequestResponse(
  res: NextApiResponse,
  message = 'Invalid request'
) {
  res.status(400).json({
    error: message,
  })
}
