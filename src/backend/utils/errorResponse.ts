import { AxiosError } from 'axios'
import { NextApiResponse } from 'next'
import logger from './logger'

export default function errorResponse(
  res: NextApiResponse,
  err: AxiosError | Error
) {
  // If we received a 400 from the upstream service, pass it along
  if ('response' in err && err.response?.status === 400) {
    res.status(400).json({
      error: 'Invalid request',
    })
    return
  }
  let error: { message: string; stack?: string } = {
    message: 'An error has occurred',
  }
  if (process.env.NODE_ENV === 'development') {
    error = {
      message: err.message,
      stack: err.stack,
    }
  }
  logger.log('error', err)
  res.status(500).json({
    error,
  })
}
