import { useDirections } from '../../frontend/hooks/services'
import { useRouter } from 'next/router'
import { PageSubTitle, PageTitle } from '../../frontend/styles/typography'
import PageContainer from '../../frontend/components/page-container'
import Head from 'next/head'
import LoadingIndicator from '../../frontend/components/loading-indicator'
import MetroList from '../../frontend/components/metro-list'

export default function DirectionsPage() {
  const router = useRouter()
  const { query } = router
  const { id } = query
  const { data } = useDirections(id)
  const directions = data?.directions || []
  const items = directions.map((direction) => {
    return {
      id: direction.id,
      href: `/stops/${router.query.id}/${direction.id}`,
      content: <>{direction.name}</>,
    }
  })
  return (
    <>
      <Head>
        <title>MetroTransit - Select a Direction</title>
      </Head>
      <PageContainer>
        <PageTitle data-testid="page-title">
          Which way are you headed?
        </PageTitle>
        <PageSubTitle data-testid="page-sub-title">
          We just need a little bit more information.
        </PageSubTitle>
        {!data && <LoadingIndicator />}
        <MetroList items={items} />
      </PageContainer>
    </>
  )
}
