import Head from 'next/head'
import { useTransitRoutes } from '../../frontend/hooks/services'
import PageContainer from '../../frontend/components/page-container'
import LoadingIndicator from '../../frontend/components/loading-indicator'
import MetroList, { ListItemData } from '../../frontend/components/metro-list'
import { PageSubTitle, PageTitle } from '../../frontend/styles/typography'
import TransitRouteItem from '../../frontend/components/transit-route-item'

export default function TransitRoutes() {
  const { data } = useTransitRoutes()
  const routes = data?.routes || []
  const items: ListItemData[] = routes.map((route) => {
    return {
      id: route.id,
      href: `/directions/${route.id}`,
      content: <TransitRouteItem {...route} />,
    }
  })
  return (
    <>
      <Head>
        <title>MetroTransit - Routes</title>
      </Head>
      <PageContainer>
        <PageTitle data-testid="page-title">MetroTransit Routes</PageTitle>
        <PageSubTitle data-testid="page-sub-title">
          Select a route to view your stops.
        </PageSubTitle>
        {!data && <LoadingIndicator />}
        <MetroList items={items} />
      </PageContainer>
    </>
  )
}
