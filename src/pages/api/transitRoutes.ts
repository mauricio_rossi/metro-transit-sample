import errorResponse from '../../backend/utils/errorResponse'
import { getTransitRoutes } from '../../backend/services/metro'
import cachedResponse from '../../backend/utils/cachedResponse'

export default async (_req, res) => {
  try {
    const data = await getTransitRoutes()
    cachedResponse(res, { routes: data })
  } catch (err) {
    console.error(err)
    errorResponse(res, err)
  }
}
