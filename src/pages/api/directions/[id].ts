import errorResponse from '../../../backend/utils/errorResponse'
import { getDirections } from '../../../backend/services/metro'
import { NextApiRequest, NextApiResponse } from 'next'
import cachedResponse from '../../../backend/utils/cachedResponse'
import badRequestResponse from '../../../backend/utils/badRequestResponse'

export default async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const routeId = req.query.id
    if (typeof routeId !== 'string') {
      return badRequestResponse(res)
    }
    const directions = await getDirections(routeId)
    cachedResponse(res, {
      directions,
    })
  } catch (err) {
    errorResponse(res, err)
  }
}
