import errorResponse from '../../../backend/utils/errorResponse'
import { getStops, TransitDirection } from '../../../backend/services/metro'
import { NextApiRequest, NextApiResponse } from 'next'
import badRequestResponse from '../../../backend/utils/badRequestResponse'
import cachedResponse from '../../../backend/utils/cachedResponse'

export default async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const { slug } = req.query
    if (slug?.length !== 2) {
      return badRequestResponse(res)
    }
    const id = slug[0]
    const direction = TransitDirection[slug[1]]
    if (!direction) {
      return badRequestResponse(res)
    }
    const stops = await getStops(id, direction)
    cachedResponse(res, { stops })
  } catch (err) {
    errorResponse(res, err)
  }
}
