import Head from 'next/head'
import styled from 'styled-components'
import { useStops } from '../../frontend/hooks/services'
import { useRouter } from 'next/router'
import { PageSubTitle, PageTitle } from '../../frontend/styles/typography'
import PageContainer from '../../frontend/components/page-container'
import LoadingIndicator from '../../frontend/components/loading-indicator'

export default function StopsPage() {
  const router = useRouter()
  const { query } = router
  const { slug } = query
  const { data } = useStops(slug?.[0], slug?.[1])
  return (
    <>
      <Head>
        <title>MetroTransit - Your Route&apos;s Stops</title>
      </Head>
      <PageContainer>
        <PageTitle data-testid="page-title">Your Route&apos;s Stops</PageTitle>
        <PageSubTitle data-testid="page-sub-title">
          Your route stops at the following places:
        </PageSubTitle>
        {!data && <LoadingIndicator />}
        <Stops>
          {data?.stops?.map((stop) => (
            <Stop key={stop.id}>{stop.name}</Stop>
          ))}
        </Stops>
      </PageContainer>
    </>
  )
}

const Stops = styled.ul`
  margin: 15px 0;
  padding: 0 0 0 5px;
  position: relative;
`

const Stop = styled.li`
  display: flex;
  align-items: center;
  padding: 0 0 20px 25px;
  font-weight: regular;
  text-decoration: none;
  position: relative;
  line-height: 1.3;
  &:before {
    content: ' ';
    width: 2px;
    position: absolute;
    top: 0;
    height: 100%;
    left: 0;
    background: #c1dcfd;
  }
  &:after {
    content: ' ';
    width: 10px;
    height: 10px;
    position: absolute;
    top: 0;
    left: -5px;
    background: white;
    border: 1px solid #001a3a;
    border-radius: 50%;
  }
  &:last-child {
    &:before {
      display: none;
    }
  }
`
