import Head from 'next/head'
import GlobalStyles from '../frontend/styles/global-styles'

function MyApp({ Component, pageProps }: any) {
  return (
    <>
      <GlobalStyles />
      <Head>
        <meta
          name="viewport"
          content="initial-scale=1, viewport-fit=cover, width=device-width"
        />
      </Head>
      <Component {...pageProps} />
    </>
  )
}

export default MyApp
