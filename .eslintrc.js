module.exports = {
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:prettier/recommended',
  ],
  parser: '@typescript-eslint/parser',
  plugins: ['@babel', 'jest'],
  globals: {
    JSX: 'readonly',
    Express: 'readonly',
    React: 'writable',
  },
  env: {
    node: true,
    jest: true,
    browser: true,
    commonjs: true,
    es6: true,
  },
  rules: {
    'react/react-in-jsx-scope': 'off',
    'react/prop-types': [2, { ignore: ['children'] }],
    'no-unused-vars': 'off',
  },
  settings: {
    react: {
      version: 'detect',
    },
  },
}
